# Moderators

---

## Craig Maloney (aka cmaloney)

<img src="https://framagit.org/uploads/-/system/user/avatar/4774/avatar.png" align="left" alt="Cmaloney's avatar on Framagit" width="100"/>

Craig Maloney has been a fan of computers since he first learned about them from the World Book Encyclopedia (remember those?). By day he programs computers and by night he also programs computers. He is the maintainer of the Pepper&Carrot Wiki where he works to document and expand the world of Hereva for others to use in their project, including a long-delayed RPG that he's working on. He is the host of [Open Metalcast](http://openmetalcast.com) and is one of the board members for the [Michigan!/usr/group](http://mug.org). Learn more at his [site](http://decafbad.net).

**Contact:** [Private message on IRC](http://webchat.freenode.net/?channels=%23pepper%26carrot).

---

## David Revoy (aka deevad)

<img src="https://framagit.org/uploads/-/system/user/avatar/5950/avatar.png" align="left" alt="Deevad's avatar on Framagit" width="100"/>

David Revoy is a French artist born in 1981 and the author of Pepper&Carrot. He is passionate about drawing, painting, cats, computers, GNU/Linux, Free/Libre and Open-source Culture, Internet, old school RPG video games, old manga and anime, traditional art, Japanese culture, fantasy… He also likes to share tutorials and making-ofs on his [blog](https://www.davidrevoy.com).

**Contact:** [Private message on IRC](http://webchat.freenode.net/?channels=%23pepper%26carrot) or [info@davidrevoy.com](mailto:info@davidrevoy.com).

---

## Midgard (aka m1dgard)

<img src="https://framagit.org/uploads/-/system/user/avatar/2885/piep-profile.png" align="left" alt="M1dgard's avatar on Framagit" width="100"/>

Midgard was born on the web and is interested in decentralisation. A computer science student, he keeps the Pepper&Carrot Git repositories tidy. He's from Belgium and works on the Dutch translations, and does general QA of other translations. Outside of Pepper&Carrot, he's an active [OpenStreetMap](https://openstreetmap.org/)per.

**Contact:** [Private message on IRC](http://webchat.freenode.net/?channels=%23pepper%26carrot) or @midgard on [Framateam](https://framateam.org/peppercarrot/channels/town-square).

---

## Valvin (aka valvin)

<img src="https://framagit.org/uploads/-/system/user/avatar/2861/avatar.png" align="left" alt="Valvin's avatar on Framagit" width="100"/>

Valvin was born in France in 1981. He also speaks English but makes a lot of mistakes. Passionate about Free/Opensource software and culture, he has a particular interest in Internet decentralization, privacy and [Framasoft](https://framasoft.org)'s campaigns. He likes drawing but still has a lot to learn. Contributions on P&C are about beta-reading and being a member of the French translators. [His blog (fr)](https://blog.valvin.fr)

**Contact:** [Private message on IRC](http://webchat.freenode.net/?channels=%23pepper%26carrot), @valvin on [Framateam](https://framateam.org/peppercarrot/channels/town-square) or @valvin:matrix.org on [Matrix](https://riot.im/app/#/room/#pepper&carrot:matrix.org).

 

