Translate the webcomic
======================

![cover image](https://www.peppercarrot.com/data/images/lab/2015-03-01_translation.jpg)

**Translating an episode of Pepper&Carrot is fast, fun and easy!**  

You can do it from any type of computer (Windows, Mac, Linux and more) and using only free/libre tools. In this documentation you'll be able to read all information about managing your own translation of Pepper&Carrot.

Check the menu on left of this page and go to the next chapter.
