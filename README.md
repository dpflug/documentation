# Introduction:

### A documentation for contributors by contributors

This documentation is the technical reference for the contributors of Pepper&Carrot:
- **Translators**
- **Developers**
- **Creators of derivations**

The purpose of this wiki is to centralize all the technical information for you to be independent. I hope you'll find it useful.

**Dynamic documentation**

This documentation is dynamic: everyone who has an account on Framagit can propose changes to this wiki by opening a Merge Request on the [Git repository here](https://framagit.org/peppercarrot/documentation). Trusted people might receive developer permissions, they can edit the repository directly.

For more information about how to register on Framagit and get developer permissions, read [Method using a Web browser](index.php?en/static14/documentation&page=040_-_Method_using_a_Web_browser), chapter 3, part a 'Connect with us'.
