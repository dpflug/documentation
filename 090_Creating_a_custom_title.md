Custom episode header
=====================

Tharinda Divakara created a tutorial about how to create a cool vector title with Pepper&Carrot style. You can [read it here](https://www.peppercarrot.com/data/images/lab/2015-02-21_Multilingue-SVG-researches/2015-03-04_Sinhala-title-translation_how-to_by-Tharinda-Divakara.jpg "Tutorial about creating vector titles in Pepper&Carrot style").
