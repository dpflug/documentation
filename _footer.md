*(Documentation created on August 2018; work in progress)*  
**Editing:** David Revoy.   
**Correction:** Midgard, Sitelen.   
**API documentation:** Jookia.   

![CC-By license image](https://www.peppercarrot.com/data/wiki/medias/img/logo_cc.png)  
[Creative Commons Attribution, CC-By](https://creativecommons.org/licenses/by/4.0/)
