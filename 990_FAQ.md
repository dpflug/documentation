Translation F.A.Q.
==================

**Q: When I open a SVG, Inkscape prompt me with a dialog window at start-up "Convert Legacy Inkscape file", what should I do?**  
A: The one looking like looking like [that](https://www.peppercarrot.com/data/documentation/medias/img/convert-legacy-inkscape-file.jpg)? Just keep the default option "This file contains digital artwork for screen display. (Choose if unsure)" and then press the OK button.

**Q: A sentence doesn't sound good in my language, can I rewrite it?**  
A: That's ok. The most important is to keep the semantic, the feeling and the information of the story. Adding style and changing the expression of your target language is natural.

**Q: Do I need to translate all the episodes to get published?**  
A: No. Feel free to contribute within your possibilities: you can translate just one episode then try one another later. Take your time. The website is designed to fall back to English if there's no translation available for a given episode. So, you can send episode 01, then one month later send episode 03, 04, 05 in a row, then stop here if you want... or then send all the other episodes. Every contribution is welcome; thank you for your effort and contributions!

**Q: I translated already many episodes, but the language I maintain still is not appearing in the lang list on the homepage? Why**  
A: Yes, homepage (and other pages of the website) use the website translation file (a PHP file), while episodes uses their own translation files (the SVGs). If you want the button on the homepage, please [translate the website using this tutorial](https://www.peppercarrot.com/static14/documentation&page=110_Website_translation).

**Q: Can I translate fictive language, as Klingon, Dothraki, Quenya, Sindarin?**  
A: Yes and No. Yes because you are free to translate the SVG and publish them on your blog/forum as a cross-over with a fair-use "fan-art" of Copyrighted material. 
No, if you plan to get this files accepted into the main repository of Pepper&Carrot because it is not Creative Commons compatible. You can read more about it on [this thread](https://framagit.org/peppercarrot/webcomics/issues/60).

**Q: A SVG is missing in the translation folder; at episode four: E04P05.svg!**  
A: The file is indeed missing on purpose: in the comic this panel is a GIF animation and can't be translated. I had to remove the SVG for this page so the renderfarm ignore computing a translation for this specific case. A special set of rules are applied each time a GIF animation appears in the story. That's why it is a rare situation.

**Q:** **Where can I get an overview of all available translations?**  
A: We maintain [a big table](https://www.peppercarrot.com/en/static6/sources&page=translation) on the official website to get an overview of the translation efforts.
