Translating characters names
============================

**Is it possible to change the characters' names for a translation?**

Sure, you can. For the French translation, I kept the english 'Carrot' because the French name "Carotte" is feminine. I kept 'Pepper' and didn't use the translation 'Poivre' or 'Poivron' because it sounds like an insult French-speakers may throw at a drunkard, 'Poivrot!'. 

**The reference file**

To keep track of all international names while you translate the episodes, you can fill this spreadsheet ["translation-names-references.fods"](https://www.peppercarrot.com/0_sources/translation-names-references.fods) at the root of the webcomics repository. It's a \*.fods file you can open with [LibreOffice Calc](https://www.libreoffice.org/). 

You can save your modification to this file and send it back on the webcomics repository the same way as you would do for proposing a translation. More information on editing and uploading a file on the ["Method using a Web browser"](https://www.peppercarrot.com/static14/documentation&page=040_-_Method_using_a_Web_browser) page.

