The necessary tools
===================

All you'll need to get started is a **Web browser**, **Inkscape** and **the fonts of the project**. Here is how you can get them and install them:

## The web browser

We recommend [Firefox](https://www.mozilla.org/firefox/) but you can use the web browser of your choice: e.g. Chrome, Safari, Edge will work too.

## Inkscape

[Inkscape](https://inkscape.org/en/) is a free/libre and open-source vector drawing program for Windows, macOS and GNU/Linux. We use it for editing the texts, sound effects and speech bubbles.

**Windows and macOS**
We advice you to download a fresh copy of Inkscape from the official website. [Download Inkscape](https://inkscape.org/en/release/). The installation instruction are on the Inkscape page.

**GNU/Linux**
You can install Inkscape via the repository of your distribution. You need **0.92.3 or newer** or 0.91. Avoid 0.92, 0.92.1, 0.92.2, because of bugs.

## The fonts

Pepper&Carrot uses a lot of different open fonts. You might not need to install all of them (eg. Malayalam, Chinese Simplified or Korean subfolder). But if you plan to translate from the English version, you'll need to install all the fonts in the Latin subfolder to be able to read the English version as it's intended. Here is how to install the font for **Windows**, **macOS** and **Linux**.

**Install fonts on Windows**

* [Download peppercarrot-fonts-latest.zip](https://www.peppercarrot.com/0_sources/0ther/tools/zip/peppercarrot-fonts-latest.zip)
* Extract peppercarrot-fonts-latest.zip to a temporary folder.
* Open your Control Panel.
* Go into Appearance and Personalization.
* Open Fonts.
* Your folder font will open.
* Drag and drop the Pepper&Carrot fonts into the Fonts window.
* You can now remove your downloaded zip file and the folder you temporary extracted them.

**Install fonts on Mac OSX**

* [Download peppercarrot-fonts-latest.zip](https://www.peppercarrot.com/0_sources/0ther/tools/zip/peppercarrot-fonts-latest.zip)
* Extract peppercarrot-fonts-latest.zip to a temporary folder.
* Open Finder.
* Open Applications in the sidebar.
* Select the Font Book application.
* Drag and drop the Pepper&Carrot fonts into Font Book window.
* You can now remove your downloaded zip file and the temporary folder where you extracted them.

**Install fonts on GNU/Linux**

* [Download peppercarrot-fonts-latest.zip](https://www.peppercarrot.com/0_sources/0ther/tools/zip/peppercarrot-fonts-latest.zip)
* Extract peppercarrot-fonts-latest.zip to a temporary folder.
* Open your file manager.
* Show hidden files.
* Navigate to ~/.local/share/fonts (create the folder if necessary)
* Drag and drop the Pepper&Carrot fonts into ~/.local/share/fonts.
* You can now remove your downloaded zip file and the folder you temporary extracted them.

For users or Arch Linux and derivatives: you can easily install all [Pepper&Carrot fonts from the AUR](https://aur.archlinux.org/packages/peppercarrot-fonts/).
