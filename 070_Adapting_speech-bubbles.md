Adapting the geometry of speech-bubbles
=======================================

**Padding**

Keep a good looking padding distance around the text; a text touching the edge of the speechbubble doesn't look good.

**How**

The arrow tool on the top of the vertical toolbar of Inkscape will allow you to deform the edges of the speechbubble. You can adapt the geometry of the speechbubble to match your translation (sometime translation needs larger speechbubble, or smaller).

**About**

All speechbubbles are white geometric shapes made of vector point composed of two parts: the body (the bubble) and the tail. Because both element are white or use the same color and borderless they merge visually into a single shape. 
