Italic, Bold and text-effects
=============================

Select only the text you want to make bold, then change the font style to the bold one. This [picture](https://www.peppercarrot.com/data/images/faq/2018-07-01_bold-Lavi-fix.png) shows how to do that.

If the font you are using doesn't have a bold variant (for example the old Lavi), you can select the text and add a StrokePaint to it, and change the StrokeStyle width. Here is a [picture](https://www.peppercarrot.com/data/images/faq/2018-02-22_bold-text-faq.jpg) showing how I'm doing it.
