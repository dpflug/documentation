Custom fonts
============

You can add [to the Pepper&Carrot Framagit repository](https://framagit.org/peppercarrot/webcomics) new fonts for your language. 

**Requirements:**

The fonts need to be published under a free license or released in the public domain.  
The following font licenses are accepted:

* Public domain fonts
* GNU/GPL fonts
* CC-0 fonts
* CC-BY fonts
* SIL Open Font License (OFL)

**Font information**

To ease the storage of the font on the project and its maintenance, it is convenient to also keep the following information in the repository:

* Author name or nickname of the font (File Fontname.COPYRIGHT)
* Link to the source website distributing the font. (File Fontname.COPYRIGHT)
* License file itself, txt version. (File Fontname.LICENSE)

For the formatting, look how it is done for [other fonts in the repository here](https://framagit.org/peppercarrot/webcomics/tree/master/fonts/Latin). 

**Where:**

Sources for finding new open fonts:

* [Font Library](https://fontlibrary.org/)
* [The League of Moveabletype](https://www.theleagueofmoveabletype.com/)
* [Fontspace](http://www.fontspace.com/category/open) with category “Open”
* [Font Squirrel](https://www.fontsquirrel.com/)
