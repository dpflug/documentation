Method using a web browser
==========================

Here is an easy method designed for everyone, even users without an advanced technical knowledge. This documentation is detailed and explains the process step by step.

## 1. Get the sources

1. From the menu on the website, go to [Sources](https://www.peppercarrot.com/static6/sources).
2. In the part "Source Center", click the first thumbnail *"Webcomic"*.
3. On the new page, click the episode you want to translate, e.g. "ep01 : Potion of Flight".
4. Click the "Translator's pack" button.
5. The download of the zip will start.
6. Decompress the zip file to a folder of your choice.
7. Remove the zip file to clean-up.

**Congratulations!** You now have the translator's pack of a Pepper&Carrot episode on your disk!

*Note: If you have a fast internet connection and enough disk space, you can also in one click [download all episodes](https://framagit.org/peppercarrot/webcomics/repository/master/archive.zip) instead of downloading the episodes one by one.*

## 2. Create or edit a translation

**Create a new language**

Every episode source folder has a subdirectory called ``lang``. This folder contains all languages that are already available for the episode. Duplicate an existing language using Copy/Pasting (eg. "en" folder for the English version) and rename it to the language you want to translate.

On Pepper&Carrot we use two letters only. We try to use [ISO language codes](http://www.w3schools.com/tags/ref_language_codes.asp), but if your target language doesn't have a two letter ISO code, we can invent one.

![screenshot of duplicating a folder (copy/paste), then renaming it to a fictive language named 'my'](https://www.peppercarrot.com/data/images/lab/2015-03-02_tuto-translation/2015-03-01_d_duplicate-your-folder.jpg)  
_screenshot of duplicating a folder (copy/paste), then renaming it to a fictive language named 'my'_

**Edit the translation**

Inside the folder you just created, just edit all the SVG files with Inkscape.

*Note: If Inkscape prompts you with a dialog window at start-up "Convert Legacy Inkscape file" looking like [that](https://www.peppercarrot.com/data/documentation/medias/img/convert-legacy-inkscape-file.jpg), just keep the default option "This file contains digital artwork for screen display. (Choose if unsure)" and then press the OK button.*

Translate the content of speech bubbles, change the sound effects, resize speech bubbles to make them look good with your new content. You'll find more Inkscape tips in this documentation (in the menu on the left and even in the F.A.Q).

Feel free to be the art director of your translation: resize stuff and arrange the layout to your taste. Don't forget to fill in the credits for your translation on the credit page (the last page).

## 3. Upload your translation

When your translation is complete and looks good, it is time to send it to the Pepper&Carrot official website. Before doing that be sure you agree with [the terms and conditions](https://www.peppercarrot.com/static14/documentation&page=940_license_terms_and_conditions). Take a minute, read it.

Back already? **Thank you** for agreeing to share your work under the Creative Commons Attribution license! Now we need to get some security things out of the way: because everyone can modify the files of Pepper&Carrot and propose a translation, it was necessary for the project to develop a mechanism to protect all the project and a way for every new translation to be reviewed before being exposed publicly on the website. Here is how you can send us your work:

**a. Connect with us**

1. [Open a free account on Framagit](https://framagit.org/users/sign_in?redirect_to_referer=yes). Framagit is the collaborative tool we used for the file management and bugs. It is hosted by the French association [Framasoft](https://framasoft.org/en/).
2. When your profile is activated let us know about your nickname and the project you are working on. You can write all of that in a [New issue report here](https://framagit.org/peppercarrot/webcomics/issues/new?issue)
3. Then we will notice your message, discuss and will grant you permission to upload your files to the source. You'll join the [long list of Members](https://framagit.org/groups/peppercarrot/-/group_members) as a Developer!

**b. Create a Branch**

1. Open [the Branches page](https://framagit.org/peppercarrot/webcomics/branches). Or you can find this page by clicking "Branches" on the header of the repository https://framagit.org/peppercarrot/webcomics .
2. Click the top-right colored button "New branch".
3. Setup your new branch. The branch name is a name up to your choice, just think of something without spacing or any special characters *(eg. ep26-fr, fr-corrections, mynickname-fr)*. 
4. Leave other option unchanged: "Create from master".
4. click the button "Create branch" on bottom-left to complete the creation.

**c. Add your files to the branch**

1. Open the page of your branch. This one is listed [on the Branches page](https://framagit.org/peppercarrot/webcomics/branches). 
2. Click on the title of your branch in bold.
3. You'll see a page with a big listing of all the sources files of Pepper&Carrot.
4. Click on the episode you translated *(eg. ep01_Potion-of-Flight)*, then on the subfolder ``lang``.
5. On top-left of this listing, you'll see the name of your branch. On the right of this name, you'll see a button [+] (with a plus symbol on it) looking like [that](https://www.peppercarrot.com/data/documentation/medias/img/framagit-plus-button-location.jpg). Click it.
6. Select "new directory" and then enter the two letters of your translation folder (eg. fr) and leave Commit message and Target Branch field unchanged. Validate with the colored button on the bottom-left corner "Create a directory".
7. Inside the new create folder, press again the button [+] (with a plus symbol on it) and this time select "Upload File" and enter in this one the Inkscape SVG files of your translation. Finish by the colored button "Upload file".
8. (repeat the last step to upload all the Inkscape SVG files of your translation)

**d. Create a Merge-Request to submit your modification**

1. On the top of your branch (listed in the [branch page](https://framagit.org/peppercarrot/webcomics/branches)), press the button named "Create a Merge Request" and then fill the form; you can add a Title to the change you did and then write a longer description.
2. When you added your title and description, leave all the other part unchanged and scroll down to the colored button "Submit merge request".

**Congratulation!** You sent a translation to the Pepper&Carrot team!

After that, the Pepper&Carrot team will be notified to read your Merge Request title, description and also will be able to see the files you added or changed. It is a perfect time for us to discuss and review the changes before accepting them in the official code. Framagit will ease this part because everyone in the project can comment on the Merge-Request page.

**e. In case of validation**

If everything is fine, the Merge Request will be accepted and the files and folder will join the main code. The branch will be deleted on the process. If you want to continue your work on the translation, repeat the process from (b.) to (d.). 

*Note: The comic page will appear within around 48h on the Pepper&Carrot website.*

**f. In case of necessary modifications**

During the discution with the team, we might notice your branch is not ready or missing something. (eg. a speechbubble is missing). If you want to edit, delete or modify the merge request; just:
1. Connect to [your branch](https://framagit.org/peppercarrot/webcomics/branches) 
2. Continue to work on the files on it using the button [+] (with a plus symbol on it), the Delete or the Edit button on top the SVG files. Framagit offers many options.
3. That's all: The changes will appear automatically on the Merge-request discussion thread (all Merge-request are [listed here](https://framagit.org/peppercarrot/webcomics/merge_requests) if you can't find it).
