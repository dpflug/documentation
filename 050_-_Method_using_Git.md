Webcomic translation: Advanced method using command-line and Git
================================================================

This method is for advanced user using the command-line tool Git. Our repository named "webcomic" has master branch protected: you'll need to submit your work on a branch and create a merge-request. Here is a workflow example.

## Prerequisites

This tutorial assumes you have:

* very basic knowledge of using a terminal (you should be able to use `cd` and `ls`).
* on Linux, macOS or other UNIXes: Git as a command in your terminal (to test: execute `git --version`: if it outputs something like `command not found`, you need to install Git first).
* on Windows: [Git BASH for Windows](https://gitforwindows.org/). You can do the same things with Git GUI for Windows, but this tutorial focuses on the BASH version.

## Workflow for creating a translation

### Case 1: If you don't have a local copy of the repository yet

**1. Get the sources**

Clone all the repository (all sources = 1,5GB on disk)

``git clone https://framagit.org/peppercarrot/webcomics.git``

**2. Create or edit your translation**

Enter the repository folder:

``cd webcomics``

### Case 2: If you have a local copy of the repository

**Update the sources**

When a new episode appears, you'll want to update the repository.

``git checkout master``

``git pull``

### Every time

Create and switch to a new branch (`YOURBRANCHNAME` is a name of your choice here, e.g. `ep26-fr`, `fr-corrections`, `mynickname-fr`. No spaces, no uppercase letters).

``git checkout -b YOURBRANCHNAME``

Do your changes translation changes on the SVG. For more details, I report you to the previous chapter, second part.

Check the status of your changes (note: in red, file detected changed. You can check at any step)

``git status``

You can validate your changes to a file by "adding them" with git:

``git add PATH/TO/THE/FILE``

In case you want to validate and add all the changed listed by git status do:

``git add -A``

Then commit your pack of files

``git commit``

A text editor will apear to let you enter a short description of your modification. Use first line for the title. 
Use all other lines if you want to tell more details about it. Save, quit text editor. Repeat the process of adding files and commit them if you want to split your work in more than one commit.

**3. Upload your translation**

Create a Framagit account and get developper permission as described on the previous chapter at *3-a Connect with us*. When you'll get permission, you'll be able to push your branch to Framagit:

``git push -u origin YOURBRANCHNAME``

To create a "Merge request" about your work [connect to the list of branches](https://framagit.org/peppercarrot/webcomics/branches) and click on your branch title in bold then press the top-right the colored button "Create a merge Request".

Fill the ticket, Validate, and then we will receive a notifications about your work. We will review the changes, discuss it. 

*Done!*

## Other situations

**Modification and corrections**

In case of necessary modifications, you can still continue to add commits to your branch to edit the Inkscape SVG files: the merge request will auto-update with the new commit in it. When ready and reviewed/approved, the code will join master and get rendered on the website. The comic page will appear within 48h on the Pepper&Carrot website.


if something goes really wrong and you want a vanilla repository (this will discards all local work):

``git fetch origin``

``git reset --hard origin/master``
